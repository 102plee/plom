# Courses that have used Plom

These are as best we can recall.  Currently UBC Maths-centric.


## 2018-19

| Course |  Task    | Student count | Notes                            |
|--------|----------|:-------------:|----------------------------------|
| 220    | Midterm  | 350           | Very first Plom                  |
| 405    | Midterm  | 50            | Colin joins                      |
| 104    | Midterm  | 200           | MacLean section: online return   |
| 253    | Midterm  | 100     | MacLean section graded + online return |
| 253    | Midterm  | 600     | Colin all 5 sections + online return   |
| 253    | Exam     | 600     | assigned seating, grading done offline |
| 221    | Midterm  | 650           | BenW and MattC                   |
| 101    | Midterm  | 1000          | Online return                    |
| 317    | Midterm  | 100           |                                  |
| 318    | Midterm, Exam   | 100    | Omer                             |
| 264    | Quizzes  | 200           | Seckin                           |

Approx 5000 papers over 9 courses.


## 2019-Summer

| Course |  Task         | Student count | Notes   |
|--------|---------------|:-------------:|---------|
| 220    | Midterm       | 40            | Elyse   |
| 253    | Test x4, Exam | 160           | Colin   |
| 300    | Quizzes       | 50            | Seckin  |

Approx 1000 papers over 3 courses.


## 2019-2020

| Course |  Task          | Student count |  Notes         |
|--------|----------------|:-------------:|----------------|
| 100    | Midterm + Exam | 1300          | MikeB          |
| 302    | Midterm + Exam | 200           | Omer           |
| 344    | Midterm + Exam | 100           | Omer           |
| 322    | Midterm        | 50            | Nike+Abhishek  |
| 101    | Midterm        | 1300          | AnthonyW       |
| 152    | 3 Tests        | 900           | Kalle+Maricela |
| 302    | Midterm        | 175           | Yinon          |

Approx 7500 papers over 7 courses.


## 2020-2021

| Course  |  Tasks                  | # students |  Notes              |
|---------|-------------------------|:----------:|---------------------|
| 253     | 5 quizzes + exam        | 640        | 1st student-uploaded|
| 344     | 2 midterms + exam       | 80         | Omer                |
| 220     | exam                    | 460        | Seckin/Andrew       |
| 256     | exam                    | 420        | Eric/AndreasB       |
| BMEG220 | quiz x2, midterm, exam  | 120        | 1st non-math        |
| 340     | 11 homework sets        | 127        | Forest, 1st homework|
| 253S1   | 5 quizzes + exam        | 200        |                     |

Approx 8000 papers over 7 courses


## Totals

  * Approx 22000 papers in 26 courses.
